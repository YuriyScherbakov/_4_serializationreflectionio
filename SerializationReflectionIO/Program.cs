﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SerializationReflectionIO
{
    class Program
    {
        static string inputFile = ConfigurationManager.AppSettings.Get("inputFile");
        static string outputFile = ConfigurationManager.AppSettings.Get("outputFile");

        static void Main(string [] args)
        {
            Console.SetWindowSize(130,30);

            ReadFromXML(inputFile); //Load all emploees from .xml to EmployeesManager.employees[] 

            if ( EmployeesManager.employees != null )
            {
                try
                {
                    using ( StreamWriter sw = new StreamWriter(outputFile) )
                    {
                        //Serialize filtered emloees to 
                        //new .xml
                        //EmployeesManager.Select is my filter 
                        EmployeesManager.Serialize(sw, EmployeesManager.Select);
                    }
                }
                catch ( Exception e )
                {
                    Console.WriteLine("Cannot write the file:");
                    Console.WriteLine(e.Message);
                }

            }

            Console.WriteLine("Filtered and serialized to {0}:\n", outputFile);

            ReadFromXML(outputFile);//Load all emploees from new.xml to EmployeesManager.employees[] 

            Console.ReadKey();
        }

        static void ReadFromXML(string pathFile)
        {
            try
            {
                using ( StreamReader sr = new StreamReader(pathFile) )
                {
                    EmployeesManager.Deserialize(sr); //Deserealizing existing XML-file
                    Console.WriteLine("Deserealized from {0}",pathFile);
                    Console.WriteLine(EmployeesManager.TypeEmploees());//Print emploees to console. 
                }
            }
            catch ( Exception e )
            {
                Console.WriteLine("Cannot Deserealize from {0}",pathFile);
                Console.WriteLine(e.Message);
            }
        }
    }
}
