﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SerializationReflectionIO
{

    public static class EmployeesManager
    {

        public static List<Employee> employees = new List<Employee>();
        
      
        public static string TypeEmploees()
        {
            
            string s = "";

            int i = 1;
            foreach ( var employee in employees )
            {

                s += String.Format("№ {0} Last Name: {1, 10},  First Name: {2, 8},  Age: {3,3},  Department: {4,5},  Address: {5,8},  ID: {6,16}\n",
                    i,
                    employee.LastName,
                    employee.FirstName,
                    employee.Age,
                    employee.Department,
                     
                    employee.GetType().GetField("address",
            BindingFlags.NonPublic | BindingFlags.Instance).GetValue(employee),
                   
                    employee.GetType().GetField("employeeID",
            BindingFlags.NonPublic | BindingFlags.Instance).GetValue(employee)
                    );
                ++i;
            }
            return s;
        }

        public static List<Employee> Select
        {
            get
            {
                List<Employee> emp = employees.Where(a => a.Age>=25 && a.Age<=35).ToList();
                emp.Sort();
                return emp;
            }
        }

        public static void Deserialize(StreamReader reader)
        {
            XmlSerializer serialazer = new XmlSerializer(employees.GetType(),
                new XmlRootAttribute("Employees"));
            employees = ((List<Employee>)serialazer.Deserialize(reader) );
                     
        }

        public static void Serialize(StreamWriter writer,List<Employee> emp)
        {
            XmlSerializer serialazer = new XmlSerializer(employees.GetType(),
                new XmlRootAttribute("Employees"));
            serialazer.Serialize(writer,emp);
        }

    }
}
