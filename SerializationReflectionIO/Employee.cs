﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SerializationReflectionIO
{
    
    public class Employee : IXmlSerializable, IComparable<Employee>
    {
        public Employee()
        {
        }
        
        string employeeID;
        string address;

        public string FirstName
        {
            get; set;
        }
        public string LastName
        {
            get; set;
        }
        public int Age
        {
            get; set;
        }
        public string Department
        {
            get; set;
        }
        public string Address
        {
            get
            {
                return this.address;
            }
            set
            {
                this.address = value;
            }
        }

        public XmlSchema GetSchema()
        {
            return ( null );
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();
            while ( reader.Name != "Employee" )
            {
                switch ( reader.Name )
                {
                    case "FirstName":
                        FirstName = reader.ReadElementContentAsString();
                        break;
                    case "LastName":
                        LastName = reader.ReadElementContentAsString();
                        break;
                    case "Age":
                        Age = reader.ReadElementContentAsInt();
                        break;
                    case "Department":
                        Department = reader.ReadElementContentAsString();
                        break;
                    case "Address":
                        Address = reader.ReadElementContentAsString();
                        break;
                }
            }
            reader.ReadEndElement();
            this.employeeID = this.LastName + this.FirstName;
           
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("FirstName",FirstName);
            writer.WriteElementString("LastName",LastName);
            writer.WriteElementString("Age",Age.ToString());
            writer.WriteElementString("Department",Department);
            writer.WriteElementString("Address",Address);

        }

        public int CompareTo(Employee other)
        {
            if ( String.Compare(this.employeeID,other.employeeID) < 0 )
            {
                return -1;
            }
            if ( String.Compare(this.employeeID,other.employeeID) > 0 )
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
