﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SerializationReflectionIO
{
   [Serializable]
   public class Employees
    {
        public Employee [] employees;
        public Employees()
        {

        }

        void UpdateIds()
        {
            for ( int i = 0; i < employees.Length; i++ )
                employees [i].EmployeeID = employees [i].LastName + employees [i].FirstName;
        }
        public override string ToString()
        {
            string s = "";

            // Get the type of MyClass.
            Type myType = typeof(Employee);

            // Get the FieldInfo of MyClass.
            FieldInfo [] myFields = myType.GetFields(BindingFlags.NonPublic
                    | BindingFlags.Instance);

            for ( int i = 0; i < employees.Length; i++ )

                s += String.Format("№ {0} Last Name: {1, 10},  First Name: {2, 8},  Age: {3,3},  Department: {4,5},  Address: {5,8},  ID: {6,16}\n",
                    i,
                    employees [i].LastName,
                    employees [i].FirstName,
                    employees [i].Age,
                    employees [i].Department,
                    myFields [5].GetValue(employees [i]),
                    myFields [0].GetValue(employees [i])
                    );

            return s;
        }

        public Employees Select
        {
            get
            {
                return new Employees {
                    employees = 
                    ( from employee in employees
                      where employee.Age >= 25 && employee.Age <= 35
                      orderby employee.EmployeeID select employee ).ToArray()};
            }
        }

        public static Employees Deserialize(StreamReader reader)
        {
            XmlSerializer serialazer = new XmlSerializer(typeof(Employees));
            var emp = (Employees)serialazer.Deserialize(reader);
            emp.UpdateIds();
            return emp;
        }

        public void Serialize(StreamWriter writer)
        {
            XmlSerializer serialazer = new XmlSerializer(typeof(Employees));
            serialazer.Serialize(writer, this);
        }

    }
}
